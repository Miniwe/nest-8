const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://miniwe.github.io/', {waitUntil: 'networkidle2'});
  await page.pdf({path: 'minwe-github-io.pdf', format: 'A4'});

  await browser.close();
})();
