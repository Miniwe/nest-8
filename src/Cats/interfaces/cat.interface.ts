import { Document } from 'mongoose';

export interface Cat extends Document {
  readonly title: string;
  readonly description: string;
  readonly option: [string];
}
