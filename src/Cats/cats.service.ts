import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { NewCatInput as NewInput } from './dto/new-cat.input';
import { EditCatInput as EditInput } from './dto/edit-cat.input';
import { CatsArgs } from './dto/cats.args';
import { Cat } from './models/cat.model';

// union CatBoolean = Cat | Boolean;

@Injectable()
export class CatsService {
  constructor(@InjectModel('Cat') private model: Model<Cat>) {}

  async create(data: NewInput): Promise<Cat> {
    return await new this.model(data).save();
  }

  async findAll(catsArgs: CatsArgs): Promise<Cat[]> {
    const { skip, take } = catsArgs;
    return await this.model
      .find()
      .skip(skip || 0)
      .limit(take || 25)
      .exec();
  }

  async findOneById(id: string): Promise<Cat> {
    return await this.model.findOne({ _id: id });
  }

  async remove(id: string): Promise<Cat> {
    return await this.model.findByIdAndRemove(id);
  }

  async removeAll(): Promise<Cat> {
    return await this.model.deleteMany();
  }

  async update(id: string, data: EditInput): Promise<Cat | null> {
    return await this.model.findByIdAndUpdate(id, data, { new: true });
  }
}
