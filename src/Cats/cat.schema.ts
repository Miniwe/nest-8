import * as mongoose from 'mongoose';
import { generate } from 'shortid';

export const CatSchema = new mongoose.Schema({
  _id: { type: String, default: () => generate() },
  title: String,
  description: String,
  options: [String],
  createdAt: { type: Date, default: () => new Date() },
});
