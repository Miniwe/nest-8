import { NotFoundException, HttpException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import { NewCatInput } from './dto/new-cat.input';
import { CatsArgs } from './dto/cats.args';
import { Cat } from './models/cat.model';
import { CatsService } from './cats.service';

const pubSub = new PubSub();

type RemoveOneType = Cat | false;

@Resolver(of => Cat)
export class CatsResolver {
  constructor(private readonly catsService: CatsService) {}

  @Query(returns => Cat)
  async cat(@Args('id') id: string): Promise<Cat> {
    const cat = await this.catsService.findOneById(id);
    if (!cat) {
      throw new NotFoundException(id);
    }
    return cat;
  }

  @Query(returns => [Cat])
  async cats(@Args() catsArgs: CatsArgs): Promise<Cat[]> {
    return await this.catsService.findAll(catsArgs);
  }

  @Mutation(returns => Cat)
  async addCat(@Args('catData') catData: NewCatInput): Promise<Cat> {
    const cat = await this.catsService.create(catData);
    pubSub.publish('catAdded', { catAdded: cat });
    return cat;
  }

  @Mutation(returns => Cat)
  async removeCat(@Args('id') id: string) {
    // TODO: что делать если мы хотив вернуть либо старое значение Cat либо false ?
    const res = await this.catsService.remove(id);
    if (!res) {
      throw new HttpException('Cant be removed', 404);
    }
    return res;
  }

  @Mutation(returns => String || Boolean)
  async removeAll() {
    const { n, ok, deletedCount } = (await this.catsService.removeAll()) as any;
    if (ok === 1) {
      return deletedCount;
    } else {
      throw new HttpException('Cant be cleared', 404);
    }
  }

  @Mutation(returns => Cat || Boolean)
  async updateCat(
    @Args('id') id: string,
    @Args('catData') catData: NewCatInput,
  ): Promise<Cat> {
    const res = await this.catsService.update(id, catData);
    if (!res) {
      throw new HttpException('Cant be updated', 404);
    }
    return res;
  }

  @Subscription(returns => Cat)
  catAdded() {
    return pubSub.asyncIterator('catAdded');
  }
}
