import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, Length, MaxLength } from 'class-validator';
import { IsString, IsNotEmpty } from 'class-validator';

@InputType()
export class NewCatInput {
  @Field()
  @MaxLength(30)
  @IsString()
  @IsNotEmpty()
  readonly title: string;

  @Field({ nullable: true })
  @IsOptional()
  @Length(30, 255)
  description?: string;

  @Field(type => [String])
  options: string[];
}
